package books;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.ws.Endpoint;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        BookList bookList = new BookList();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(BookList.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            FileReader fileReader = new FileReader("data/books.3.xml");
            bookList = (BookList) unmarshaller.unmarshal(fileReader);
        } catch (JAXBException | FileNotFoundException e) {
            e.printStackTrace();
        }

        BookManager bookManager = new BookManager(bookList.getBooks());

        List<Book> results = bookManager.searchByTitle("pierwsza");
        for(Book result:results) {
            System.out.println(result.getId());
        }

        results = bookManager.searchByAuthor("autor3");
        for(Book result:results) {
            System.out.println(result.getId());
        }

        Book result = bookManager.searchByISBN("8");
        System.out.println(result.getId());

        zad4(bookManager);

        String serverUrl = "http://localhost:666/books";
        Endpoint endpoint = Endpoint.create(bookManager);
        endpoint.publish(serverUrl);
    }

    private static void zad4(BookManager bookManager) {
        Book ownBook = new Book();
        ownBook.setTitle("Laboratorium: Java i XML");
        ownBook.setAuthor("Mateusz Gnyp");

        try {
            bookManager.addBook(ownBook);
        } catch (Exception e) {
            e.printStackTrace();
        }

        BookList searchResultContainer = new BookList();
        List<Book> searchResult = bookManager.searchByTitle("Java");
        searchResultContainer.setBooks(searchResult);

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(BookList.class);
            Marshaller marshaller = jaxbContext.createMarshaller();

            FileWriter fileWriter = new FileWriter("lab4.xml");
            marshaller.marshal(searchResultContainer, fileWriter);
        } catch (JAXBException | IOException e) {
            e.printStackTrace();
        }
    }
}
