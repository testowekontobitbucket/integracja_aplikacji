package books;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService
public class BookManager {
    private List<Book> books;

    public BookManager() {};
    public BookManager(List<Book> books) {
        this.books = books;
    }

    @WebMethod
    public void addBook(@WebParam(name = "book") Book book) {
        if (book.getTitle().isEmpty() || book.getAuthor().isEmpty())
        {
            //throw new Exception("Book should have title and author.");
        }
        books.add(book);
    }

    @WebMethod
    public @WebResult(name = "books") List<Book> searchByTitle(@WebParam(name = "title") String title) {
        List<Book> results = new ArrayList<>();
        for (Book book:books) {
            if(book.getTitle().contains(title)) {
                results.add(book);
            }
        }
        return results;
    }

    @WebMethod
    public @WebResult(name = "books") List<Book> searchByAuthor(@WebParam(name = "author") String author) {
        List<Book> results = new ArrayList<>();
        for (Book book:books) {
            if(book.getAuthor().contains(author)) {
                results.add(book);
            }
        }
        return results;

    }

    @WebMethod
    public @WebResult(name = "book") Book searchByISBN(@WebParam(name = "isbn") String isbn) {
        Book result = null;
        for (Book book:books) {
            if(book.getIsbn().contains(isbn)) {
                result = book;
                break;
            }
        }
        return result;
    }
}